//: Playground - noun: a place where people can play

import Foundation

class Player {
    var name: String
    var currentPosition: Int = 0
    
    init(name: String) {
        self.name = name
    }
}

enum ActionType {
    case Snake
    case Ladder
}

class Board {
    let boxTarget: [Int: Int]
    let numberOfBoxes: Int
    
    init(numberOfBoxes: Int, boxTarget: [Int: Int]) {
        self.numberOfBoxes = numberOfBoxes
        self.boxTarget = boxTarget
    }
    
    func rollDice() -> Int {
        return (Int(arc4random()) % 6) + 1
    }
    
    func moveFrom(currentPosition: Int, rolledDice: Int) -> (finalPosition: Int, moveType: ActionType?) {
        let movedToPosition = currentPosition + rolledDice

        if let actionPosition = boxTarget[movedToPosition] {
            if actionPosition > 0 {
                return (actionPosition, ActionType.Ladder)
            } else {
                return (actionPosition, ActionType.Snake)
            }
            
        } else {
            return (movedToPosition, nil)
        }
    }
}

class Game {
    let board: Board
    var players: [Player] = []
    var winner: Player?
    private var numberOfRounds: Int = 0
    
    init(board: Board) {
        self.board = board
    }
    
    func hasWinner() -> (Bool) {
        return (winner != nil)
    }
    
    func playFullGame() -> () {
        while !hasWinner() {
            for player in players {
                let (finalPosition, actionType) = board.moveFrom(player.currentPosition, rolledDice: board.rollDice())
                
                if let actionType = actionType {
                    switch actionType {
                    case .Ladder:
                        print("\(player.name) has tio a ladder and moved to \(finalPosition)")
                    case .Snake:
                        print("\(player.name) has tio a snake and moved to \(finalPosition)")
                    }
                } else {
                    print("\(player.name) has moved to \(finalPosition)")
                }
                
                player.currentPosition = finalPosition
                
                if finalPosition == board.numberOfBoxes {
                    self.winner = player
                    print("\(player.name) has won the game")
                    break
                } else if finalPosition > board.numberOfBoxes {
                    player.currentPosition = 2 * board.numberOfBoxes - player.currentPosition
                }
            }
        }
    }
}

let snakeLadderBoard: Board = Board(numberOfBoxes: 100, boxTarget: [
    3: 10,
    8: 5,
    13: 23,
    55: 39,
    77: 89,
    99: 1
    ])

// Initialize game
var newGame: Game = Game(board: snakeLadderBoard)
newGame.players.append(Player(name: "PC"))
newGame.players.append(Player(name: "IVAN"))
newGame.playFullGame()


