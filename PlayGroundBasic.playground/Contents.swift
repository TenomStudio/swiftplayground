var plantHeight: Double = 30.3
func waterPlant(amountOfWater: Double) {
    if amountOfWater > 5 {
        plantHeight = 0
    } else {
        plantHeight += amountOfWater / 2
    }
}

var i = 0
while plantHeight < 50 {
    waterPlant(4)
    i++
}
print("The plant is now \(plantHeight) cm tall.")
print(i)


func rollDice(numSides: Int, numTimes: Int) -> Int {
    return numSides * numTimes
}

var retInt: Int = rollDice(2 , 3)
print(retInt)


